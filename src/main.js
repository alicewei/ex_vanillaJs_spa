import main from "./main.scss"

// Vanilla Js
const items = document.querySelector('.items');
const linkArr = document.querySelectorAll('a');

items.addEventListener('click', (e) => {
    e.preventDefault();
    let pageLink = e.target.getAttribute('href');
    getLocation(pageLink);
    callpage(pageLink);
});

// link other page: use SPA, not reload
var getLocation = (nowRef) => {
    window.history.pushState({page: nowRef}, null, nowRef);
}

// get html
var callpage = (pageLink) => {
    const xhr = new XMLHttpRequest();
    let getHtml =  pageLink + '.html';
    xhr.onreadystatechange = () => {
        if(xhr.readyState == 4 && xhr.status == 200){
            var xhrData = xhr.responseText;
            if(xhrData.result == false){
                console.log('404, xhr error');
            }else{
                document.querySelector('.content').innerHTML = xhrData;
            }
        }
    }
    xhr.open('GET', getHtml, true);
    xhr.send();
}

// click next, prev
window.onpopstate = (e) => {
    let pageState = e.state;
    (pageState !== null) ? callpage(pageState.page) : location.reload();
}